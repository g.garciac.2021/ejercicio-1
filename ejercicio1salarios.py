def salario(nombre,nomina,porcentaje_impuesto,antiguedad):
    impuesto = nomina * porcentaje_impuesto
    coste_salarial = impuesto + nomina
    if antiguedad > 1:
        ahorro_coste_salarial = coste_salarial * 0.1
        coste_salarial = coste_salarial - ahorro_coste_salarial
    return print("El empleado" , nombre, "con una nómina de" , nomina, "paga un total de impuestos:", impuesto, "y tiene un coste salarial de", coste_salarial)

nombre = input("Nombre del empleado:")
nomina = int(input("Nómina de empleado"))
porcentaje_impuesto = float(input("Introduce el porcentaje de impuesto"))
antiguedad = int(input("Introduce la antigüedad del empleado"))
salario(nombre,nomina,porcentaje_impuesto, antiguedad)

#FRASE PARA PROBAR GIT PULL
